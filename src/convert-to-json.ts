import { ZoomChatEntry } from "./definitions";

export const zoomChatlogToObject = (zoomChatlog: string): ZoomChatEntry[] => {
    const entries: ZoomChatEntry[] = [];
    const lines = zoomChatlog.split("\n");
    for (const line of lines) {
        const [timestamp, afterTimestamp] = line.split(
            "\u0009\u0020From\u0020"
        );
        const [username, message] = afterTimestamp.split("\u0020\u003a\u0020");
        const entry: ZoomChatEntry = {
            timestamp,
            username,
            message,
        };
        entries.push(entry);
    }
    return entries;
};

export const zoomChatlogToJsonString = (zoomChatlog: string): string => {
    return JSON.stringify(zoomChatlogToObject(zoomChatlog));
};
