export type ZoomChatUsername = string;
export type ZoomChatTimestamp = string;
export type ZoomChatMessage = string;

export interface ZoomChatEntry {
    timestamp: ZoomChatTimestamp;
    username: ZoomChatUsername;
    message: ZoomChatMessage;
}
