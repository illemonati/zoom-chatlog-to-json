export declare type ZoomChatUsername = string;
export declare type ZoomChatTimestamp = string;
export declare type ZoomChatMessage = string;
export interface ZoomChatEntry {
    timestamp: ZoomChatTimestamp;
    username: ZoomChatUsername;
    message: ZoomChatMessage;
}
