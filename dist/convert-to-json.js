"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zoomChatlogToJsonString = exports.zoomChatlogToObject = void 0;
var zoomChatlogToObject = function (zoomChatlog) {
    var entries = [];
    var lines = zoomChatlog.split("\n");
    for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
        var line = lines_1[_i];
        var _a = line.split("\u0009\u0020From\u0020"), timestamp = _a[0], afterTimestamp = _a[1];
        var _b = afterTimestamp.split("\u0020\u003a\u0020"), username = _b[0], message = _b[1];
        var entry = {
            timestamp: timestamp,
            username: username,
            message: message,
        };
        entries.push(entry);
    }
    return entries;
};
exports.zoomChatlogToObject = zoomChatlogToObject;
var zoomChatlogToJsonString = function (zoomChatlog) {
    return JSON.stringify(exports.zoomChatlogToObject(zoomChatlog));
};
exports.zoomChatlogToJsonString = zoomChatlogToJsonString;
