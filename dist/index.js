"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zoomChatlogToJsonString = exports.zoomChatlogToObject = void 0;
var convert_to_json_1 = require("./convert-to-json");
Object.defineProperty(exports, "zoomChatlogToObject", { enumerable: true, get: function () { return convert_to_json_1.zoomChatlogToObject; } });
Object.defineProperty(exports, "zoomChatlogToJsonString", { enumerable: true, get: function () { return convert_to_json_1.zoomChatlogToJsonString; } });
