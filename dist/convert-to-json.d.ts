import { ZoomChatEntry } from "./definitions";
export declare const zoomChatlogToObject: (zoomChatlog: string) => ZoomChatEntry[];
export declare const zoomChatlogToJsonString: (zoomChatlog: string) => string;
